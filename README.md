# Szkola DevOps infrastructure

The infrastructure is built using the AWS products.

## Table of Contents
- [Variables](#variables)
- [Working with the infrastructure](#working-with-the-infrastructure)

## Variables

All the variables that you may need when working with the infrastructure are listed in the `.env.dist` file.
Copy the file to `.env` and set its values when needed. `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` you can find in the AWS Console.

## Working with the infrastructure

The infrastructure is managed by Terraform. All required tools are prepared in the docker image.
`Tools` can be started by:

        docker-compose run --rm tools

or

        docker run --rm -it --env-file .env -v $(pwd):/infrastructure registry.gitlab.com/szkola-devops/tools:v2 /bin/bash
        cd /infrastructure

### Initialize the Terraform environment

After running `Tools` the first command that should be run for any new or existing Terraform configuration is:

        terraform init

This sets up all the local data necessary to run Terraform.