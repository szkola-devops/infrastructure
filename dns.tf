resource "aws_route53_zone" "kd_zsf" {
  name = "kd-zsf.szkoladevops.com"
}

output "kd_zsf_ns" {
  value = "${aws_route53_zone.kd_zsf.name_servers}"
}

resource "aws_route53_record" "instance" {
  zone_id = "${aws_route53_zone.kd_zsf.zone_id}"
  name    = "kd-zsf.szkoladevops.com"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.instance.public_ip}"]
}