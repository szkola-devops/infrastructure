provider "aws" {
  version = "2.31.0"
}

terraform {
  backend "s3" {
    encrypt        = "true"
    bucket         = "kd-zsf-states"
    key            = "infrastructure.tfstate"
    dynamodb_table = "terraform_lock"
    region         = "eu-central-1"
  }
}

resource "aws_key_pair" "admin" {
  key_name   = "admin public key"
  public_key = "${file("ssh/admin.pub")}"
}
