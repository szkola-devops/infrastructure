provider "aws" {
    version = "2.31.0"
}

resource "aws_s3_bucket" "bucket_for_terraform_state" {
    bucket = "kd-zsf-states"

    versioning {
        enabled = true
    }
}

resource "aws_dynamodb_table" "terraform_lock" {
    name = "terraform_lock"
    hash_key = "LockID"
    write_capacity = 5
    read_capacity = 5
    attribute {
        name = "LockID"
        type = "S"
    }
}
