resource "aws_instance" "instance" {
  availability_zone = "eu-central-1b"
  ami               = "ami-038cea5071a5ee580"
  instance_type     = "t2.nano"
  key_name          = "${aws_key_pair.admin.key_name}"

  root_block_device {
    volume_size = "10"
  }

  vpc_security_group_ids = ["${aws_security_group.http_ssh.id}"]

  tags = {
    Name = "Test instance from SD course"
  }
}

data "template_file" "inventory" {
  template = "${file("templates/inventory.tpl")}"
  vars = {
    ip = aws_instance.instance.public_ip
  }
}

resource "null_resource" "update_inventory" {
  triggers = {
    template = data.template_file.inventory.rendered
  }
  provisioner "local-exec" {
    command = "echo '${data.template_file.inventory.rendered}' > ansible/inventory"
  }

  provisioner "local-exec" {
    command = "cd ansible/ && ansible-playbook -i inventory application.yml"
  }
}

output "instance_public_ip" {
  value = "${aws_instance.instance.public_ip}"
}
